const DTP_API_PATH = '../../dtp-api/';
const PORT = require('../../dtp-ecosystem/reference/port');
const ENV = process.env.NODE_ENV;

module.exports = {
  bot: {
    token: ''
  },
  handlers: require('./system/handlers'),
  mongo: {
    db: 'DTP-Database',
    models: require(DTP_API_PATH + 'config/system/mongo-models'),
    strict: true,
    log: process.env.LOG_LEVEL || 'info'
  },
  services: {
    api: {
      url: 'http://127.0.0.1:' + PORT['dtp-api'][ENV]
    },
    dtp: {
      url: ''
    },
    shortHost: {
      url: ''
    }
  }
};
