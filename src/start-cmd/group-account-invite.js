const $Users = require('../modules/users');
const $API = require('../modules/api');

module.exports = async function (ctx, match) {
  const update = ctx.update;
  const userId = update.message.from.id;
  const [data, inviteId] = match;

  const token = await $Users.getToken(userId);
  
  const result = await $API.query('inviteAccountMember/' + inviteId, {
    token
  });

  if (result.ok) {
    return ctx.reply(`You have successfully joined to group account!`);
  } else {
    if (result.error.code === 8) {
      // many accounts
      ctx.reply('You have exceeded the limit of allowed accounts. Please leave or delete unnecessary ones.');
    } else if (result.error.code === 10) {
      // many members
      ctx.reply(`You can't join a group account, because it has exceeded the limit of members.`);
    } else if (result.error.code === 9) {
      ctx.reply('Invalid invite!');
    } else {
      throw result;
    }
  }
};