const $Users = require('../modules/users');
const $API = require('../modules/api');
const $config = require('config');

const Markup = require('telegraf/markup');

module.exports = async function (ctx, match) {
  const update = ctx.update;
  const userId = update.message.from.id;
  const [data, down, shortId] = match;
  const delta = down ? -1 : 1;

  const token = await $Users.getToken(userId);
  const rateResult = await $API.query('ratePost', {
    token,
    delta,
    shortId
  });

  if (rateResult.ok) {
    const postURL = `${$config.get('services.shortHost.url')}/${shortId}`;

    await ctx.reply(postURL);
    return ctx.reply(`Publication rating changed by ${delta} point!`, Markup.inlineKeyboard([
      {
        text: 'Share',
        switch_inline_query: postURL
      }
    ]).extra());
  } else {
    throw rateResult;
  }
};