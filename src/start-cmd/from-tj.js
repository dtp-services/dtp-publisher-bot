const $Telegram = require('telegraf/telegram');
const $config = require('config');
const $ctx = require('../modules/ctx');

const telegram = new $Telegram($config.get('bot.token'));
const feedbackChat = -275697038;

module.exports = async function (ctx, match, extra) {
  const query = $ctx.parse(ctx);

  if (query.bot || !query.user) {
    return telegram.sendMessage(feedbackChat, 'Старт бота инициировал другой бот. Или что-то другое. [TJournal]');
  }

  const user = query.user;

  telegram.sendMessage(feedbackChat, `Из TJournal перешел ${user.first_name}${user.last_name ? ' ' + user.last_name : ''} ${user.username ? '@' + user.username : ''}`);

  await extra.startMessage();
};