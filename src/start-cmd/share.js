const $Users = require('../modules/users');
const $API = require('../modules/api');
const $config = require('config');
const Markup = require('telegraf/markup');

const shortHost = $config.get('services.shortHost.url');

module.exports = async function (ctx, match) {
  const update = ctx.update;
  const userId = update.message.from.id;
  const [data, shortId] = match;

  const link = shortHost.replace(/http(s)?:\/\//, '') + '/' + shortId;
  const text = link;

  ctx.reply(text, Markup.inlineKeyboard([
    [
      {
        text: 'Choose conversation to Share',
        switch_inline_query: link
      }
    ]
  ]).extra());
};