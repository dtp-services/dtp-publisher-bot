const $Users = require('../modules/users');
const $API = require('../modules/api');

const authHandler = require('../handlers/auth-handler');

module.exports = async function (ctx, match) {
  authHandler(ctx);
};