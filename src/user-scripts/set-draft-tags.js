const $Posts = require('../modules/posts');
const $validator = require('jsonschema').validate;
const $UserScript = require('../modules/user-script');
const $log = require('../libs/log');
const $config = require('config');
const Markup = require('telegraf/markup');

const _ = require('lodash');
const CMD_SEPARATOR = '<!-!>';
const shortHost = $config.get('services.shortHost.url').replace(/http(s)?:\/\//, '');

const TAGS_SCHEMA = {
  "type": "array",
  "maxItems": 10,
  "minItems": 1,
  "items": {
    "type": "string",
    "pattern": "^#?([а-яА-Я\\w0-9])+$"
  }
};

module.exports = async function (ctx, payload) {
  const text = _.get(ctx, 'message.text', '');
  const tags = text.split(/\s+/);
  const valid = $validator(tags, TAGS_SCHEMA);

  if (valid.errors.length !== 0) {
    return ctx.reply('The message contains incorrect data or the keyword limit is exceeded.');
  }

  const draftId = payload.draftId;
  const userId = ctx.from.id;

  async function cancel (err) {
    if (err === 'POST_ALREADY_EXISTS') {
      ctx.reply('Sorry, this post already exists!');
      throw 0;
    } else if (err === 'AUTH_FAILED') {
      ctx.reply('An authentication error occurred. Contact support: durov.tech@gmail.com');
      throw 0;
    } else if (err === 'SMALL_CONTENT') {
      await $Posts.deleteDraftById(draftId).catch(cancel);
      await $UserScript.cancel(userId).catch(cancel);
      ctx.reply('This post is too small to publish.');
      throw 0;
    } else if (err === 'USER_BANNED') {
      await $Posts.deleteDraftById(draftId).catch(cancel);
      await $UserScript.cancel(userId).catch(cancel);
      ctx.reply('You can not publish materials, contact support!');
      throw 0;
    }

    $log.error(err);
    ctx.reply('Server Error..');
  }

  await $Posts.setToDraft(draftId, { tags }).catch(cancel);

  const publishDone = await $Posts.publish(draftId).catch(cancel);

  await $UserScript.cancel(userId).catch(cancel);

  const postUrl = `${shortHost}/${publishDone.shortId}`;
  const messageText = `You have successfully sent a post to DTP!\n\n${postUrl}`;

  ctx.reply(messageText, Markup.inlineKeyboard([
    [
      Markup.callbackButton('Delete', 'undo.publush.post' + CMD_SEPARATOR + draftId),
      {
        text: 'Share',
        switch_inline_query: postUrl
      }
    ]
  ]).extra());
};
