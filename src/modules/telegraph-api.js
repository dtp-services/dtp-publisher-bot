const $request = require('request');
const $Promise = require('bluebird');

class TelegraphAPI {
  static async query (method, params) {
    const query = await new $Promise((resolve, reject) => {
      $request({
        method: 'post',
        url: `https://api.telegra.ph/${method}${ params.path ? '/' + params.path : ''}`,
        json: params || true
      }, (err, response, body) => {
        if (err) {
          return reject(err);
        }

        resolve(body);
      });
    });

    return query;
  }
}

module.exports = TelegraphAPI;