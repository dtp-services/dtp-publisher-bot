const $mongo = require('../libs/mongo');
const $AF = require('../libs/action-flow');
const $API = require('./api');
const $log = require('../libs/log');

class Posts {
  static get draftDB () {
    return $mongo.collection('postDrafts');
  }

  static async createDraft (user_id, params) {
    const {path, account_id} = params;
    const AF = $AF.create({
      description: 'create draft',
      path
    });

    async function cancel (err) {
      await AF.end();

      if (err) {
        throw err;
      }
    }

    await AF.await();

    const draft = await Posts.draftDB.findOne({ path });

    if (draft) {
      return cancel('POST_IS_DRAFT');
    }

    const createdDate = Math.round((new Date()).getTime() / 1000);
    const insertObj = {
      user_id,
      path,
      createdDate,
      done: false
    };

    if (account_id) {
      insertObj.account_id = account_id;
    }

    await Posts.draftDB.insert(insertObj).catch(cancel);

    await AF.end();

    // todo: get from insert data
    return await Posts.draftDB.findOne({ path });
  }

  static async deleteDraft (path) {
    return await Posts.draftDB.deleteOne({ path });
  }

  static async deleteAllDrafts (userId) {
    return await Posts.draftDB.deleteMany({ user_id: Number(userId) });
  }

  static async deleteDraftById (id) {
    return await Posts.draftDB.deleteOne({ _id: id });
  }

  static async setToDraft (id, patchObject) {
    return await Posts.draftDB.editOne({ _id: id }, patchObject);
  }

  static async publish (draftId) {
    const $Users = require('./users');
    const draft = await Posts.draftDB.findOne({ _id: draftId });
    const token = await $Users.getToken(draft.user_id);

    const publishResult = await $API.query('dropPost', {
      path: `http://telegra.ph/${draft.path}`,
      tags: draft.tags,
      language: draft.language,
      account_id: draft.account_id,
      token
    });

    if (!publishResult.ok) {
      $log.warn(publishResult);

      if (publishResult.error.code === 1) {
        throw 'POST_ALREADY_EXISTS';
      } else if (publishResult.error.code === 2) {
        throw 'AUTH_FAILED';
      } else if (publishResult.error.code === 5) {
        throw 'SMALL_CONTENT';
      } else if (publishResult.error.code === 11) {
        throw 'USER_BANNED';
      } else {
        throw publishResult;
      }
    }

    const controlKey = publishResult.result.controlKey;

    await Posts.setToDraft(draftId, {
      controlKey,
      done: true
    });

    return Object.assign({
      shortId: publishResult.result.shortId
    }, publishResult.result.preview);
  }

  static async undoPublish (draftId) {
    const $Users = require('./users');
    const draft = await Posts.draftDB.findOne({ _id: draftId });
    const undoParams = {
      controlKey: draft.controlKey
    };

    if (draft.account_id) {
      undoParams.account_id = draft.account_id;
    }

    const token = await $Users.getToken(draft.user_id);

    undoParams.token = token;

    const undoResult = await $API.query('deletePost', undoParams);

    if (!undoResult.ok) {
      if (undoResult.error.code === 2) {
        throw 'AUTH_FAILED';
      } else {
        throw undoResult;
      }
    }

    await Posts.draftDB.deleteOne({ _id: draftId });
  }
}

module.exports = Posts;
