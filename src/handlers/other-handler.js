const $UserScript = require('../modules/user-script');
const $ctx = require('../modules/ctx');
const $log = require('../libs/log');

module.exports = function (bot) {
  const supportedTypes = [
    'text', 'audio', 'document', 
    'photo', 'sticker', 'video', 
    'voice'
  ];

  bot.on(supportedTypes, (ctx) => {
    const query = $ctx.parse(ctx);
    const userId = query.user.id;

    $UserScript.get(userId)
      .then(async (script) => {
        if (!script) {
          return;
        }

        const scriptFile = require('../user-scripts/' + script.cmd);
        await scriptFile(ctx, script.payload);
      })
      .catch((err) => {
        if (err === 0) {
          return;
        }
        
        $log.error(err);
        ctx.reply('Server Error!');
      })
  });
};