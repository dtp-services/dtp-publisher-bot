const $UserScript = require('../modules/user-script');
const $log = require('../libs/log');

module.exports = function (bot) {
  bot.command('cancel', (ctx) => {
    const userId = ctx.from.id;

    $UserScript.cancel(userId)
      .then(() => {
        ctx.reply('Done!');
      })
      .catch((err) => {
        $log.error(err);
        ctx.reply('Server Error!');
      });
  });
};