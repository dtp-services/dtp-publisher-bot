const $Posts = require('../modules/posts');
const $log = require('../libs/log');
const $UserScript = require('../modules/user-script');

const CMD_SEPARATOR = '<!-!>';

module.exports = function (bot) {
  bot.action(/^set.draft.language/, (ctx) => {
    const query = ctx.update.callback_query;
    const payload = query.data.split(CMD_SEPARATOR);
    const draftId = payload[1];
    const language = payload[2];

    $Posts.setToDraft(draftId, { language })
      .then(async () => {
        await $UserScript.set(ctx.from.id, 'set-draft-tags', {
          draftId
        });
        
        await ctx.editMessageText('Send up to 10 keywords or hash tags.\n*Example:* _science sports robots future_', {
          parse_mode: 'Markdown'
        });
      })
      .catch((err) => {
        $log.error(err);
        ctx.answerCbQuery('Server Error!');
      });
  });
};