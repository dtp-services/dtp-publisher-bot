const handler = require('./auth-handler');

module.exports = function (bot) {
  bot.command('auth', handler);
};
